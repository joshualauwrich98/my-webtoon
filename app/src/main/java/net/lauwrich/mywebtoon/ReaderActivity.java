package net.lauwrich.mywebtoon;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.File;

public class ReaderActivity extends AppCompatActivity {

    private String webtoon, episode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        Bundle bundle = getIntent().getExtras();
        this.webtoon = bundle.getString("webtoon");
        this.episode = bundle.getString("episode");
        Point p = new Point();
        getWindowManager().getDefaultDisplay().getSize(p);
        int deviceWidth = p.x;

        String url = "file:///storage/3533-3731/Webtoon"+ File.separator + this.webtoon + File.separator + this.episode;
        url += File.separator + this.episode + ".html";
        final WebView webView = findViewById(R.id.reader_container);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl(url);
//        webView.loadDataWithBaseURL(url, "<style> img { max-width:" + deviceWidth + "px;}</style>", "text/html", "UTF-8", null);
//        webView.getSettings().setJavaScriptEnabled(true);
//        final String css = "";
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                webView.loadUrl("javascript: (function() {" +
//                        "var parent = document.getElementsByTagName('head').item(0);" +
//                        "var style = document.createElement('style');" +
//                        "style.type = 'text/css';" +
//                        "style.innerHTML = " + css + ";" +
//                        "parent.appendChild(style);})()");
//            }
//        });

    }
}
