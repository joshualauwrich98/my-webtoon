package net.lauwrich.mywebtoon.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import net.lauwrich.mywebtoon.model.WebtoonItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class FileManager {
    private String rootPath;

    private static class FileManagerCreator {
        private static final FileManager INSTANCE = new FileManager();
    }

    public static FileManager getInstance() {
        return FileManagerCreator.INSTANCE;
    }

    private FileManager() {
    }

    public void init() {
        this.rootPath ="/storage/3533-3731/Webtoon";
    }

    public File[] getFileList(String path, boolean episode) {
        File dir = new File(path);
        ArrayList<File> files = new ArrayList<>();
        if (episode) {
            for(File file: Objects.requireNonNull(dir.listFiles())) {
                if (file.getName().contains("EPISODE") || file.getName().contains("EP.")) files.add(file);
            }

            File [] result = new File[files.size()];
            result = files.toArray(result);

            return result;
        } else {
            for(File file: Objects.requireNonNull(dir.listFiles())) {
                if (!file.getName().contains("EPISODE") && !file.getName().contains("EP.")) {
                    files.add(file);
                }
            }

            File [] result = new File[files.size()];
            result = files.toArray(result);

            return result;
        }
    }

    public WebtoonItem[] getWebtoonItem() {
        File dir = new File(this.rootPath);
        ArrayList<WebtoonItem> files = new ArrayList<>();

        for(File file: Objects.requireNonNull(dir.listFiles())) {
            files.add(new WebtoonItem(file.getName(), decodeSampledBitmapFromFile(file.getAbsolutePath() + File.separator + "cover.jpg", 150, 150)));
        }

        WebtoonItem [] result = new WebtoonItem[files.size()];
        result = files.toArray(result);

        Arrays.sort(result);

        return result;
    }

    public String[] getEpisodeList(String webtoonName) {
        File dir = new File(this.rootPath, webtoonName);
        ArrayList<String> files = new ArrayList<>();

        for(File file: Objects.requireNonNull(dir.listFiles())) {
            if (file.isDirectory()) files.add(file.getName());
        }

        String [] result = new String[files.size()];
        result = files.toArray(result);

        Arrays.sort(result);

        return result;
    }

    public String[] getEpisodeList(String webtoonName, String keyword) {
        keyword = keyword.toLowerCase();
        File dir = new File(this.rootPath, webtoonName);
        ArrayList<String> files = new ArrayList<>();

        for(File file: Objects.requireNonNull(dir.listFiles())) {
            if (file.isDirectory() && file.getName().toLowerCase().contains(keyword)) files.add(file.getName());
        }

        String [] result = new String[files.size()];
        result = files.toArray(result);

        Arrays.sort(result);

        return result;
    }

    public void temp() {

    }

    public String getRootPath() {
        return rootPath;
    }

    //    public String getProfilePicturePath() {
//        return profilePicturePath;
//    }
//
//    public String getLevelPath() {
//        return levelPath;
//    }
//
//    public String getBadgePath() {
//        return badgePath;
//    }
//
//    public String getRankPath() {
//        return rankPath;
//    }
//
//    public String getTemporaryPath() {
//        return temporaryPath;
//    }
//
//    public Bitmap getProfilePictureBitmap(String fileName) {
//        File temp = new File(this.profilePicturePath + fileName);
//        if (temp.exists()) {
//            return BitmapFactory.decodeFile(temp.getAbsolutePath());
//        }
//        return null;
//    }
//
//    public void setPlanetToJSON(PlanetList data) throws IOException {
//        File outFile = new File(levelPath, "planet.json");
//        BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
//        String newData = new Gson().toJson(data);
//        writer.write(newData);
//        writer.flush();
//        writer.close();
//    }
//
//    public PlanetList getPlanetFromJSON() throws IOException {
//        FileInputStream fis = new FileInputStream(new File(levelPath, "planet.json"));
//        InputStreamReader isr = new InputStreamReader(fis);
//        BufferedReader bufferedReader = new BufferedReader(isr);
//        StringBuilder sb = new StringBuilder();
//        String line;
//        while ((line = bufferedReader.readLine()) != null) {
//            sb.append(line);
//        }
//        return new Gson().fromJson(sb.toString(), PlanetList.class);
//    }
//
//    public int getProfilePictureSize() {
//        File [] temp = new File(profilePicturePath+File.separator).listFiles(File::isFile);
//        return temp == null ? 0 : temp.length;
//    }
//
//    public File[] getQuestion(int id) {
//        return new File(levelPath+File.separator+id+File.separator).listFiles(File::isDirectory);
//    }
//
//    public String getQuestionData(int planetId, int questionId) throws IOException {
//        FileInputStream fis = new FileInputStream(new File(FileManager.combinedPathString(true, levelPath, File.separator + planetId+"", questionId+"","right_answer.json")));
//        InputStreamReader isr = new InputStreamReader(fis);
//        BufferedReader bufferedReader = new BufferedReader(isr);
//        StringBuilder sb = new StringBuilder();
//        String line;
//        while ((line = bufferedReader.readLine()) != null) {
//            sb.append(line);
//        }
//
//        return sb.toString();
//    }
//
//    public void writeMatchLogs(String data) throws IOException {
//        File a = new File(matchLogPath, "logs.jl");
//        if (!a.exists()) {
//            a.createNewFile();
//        }
//        BufferedWriter writer = new BufferedWriter(new FileWriter(a));
//        writer.write(data);
//        writer.flush();
//        writer.close();
//    }
//
//    public static void unzipFile(File zipFile, File targetDirectory) throws IOException {
//        ZipFile zis = new ZipFile(zipFile);
//        try {
//            ZipEntry ze;
//            int count;
//            byte[] buffer = new byte[8192];
//            Enumeration<? extends ZipEntry> temp = zis.entries();
//            while (temp.hasMoreElements()) {
//                ze = temp.nextElement();
//                File file = new File(targetDirectory, ze.getName());
//                File dir = ze.isDirectory() ? file : file.getParentFile();
//                if (!dir.isDirectory() && !dir.mkdirs())
//                    throw new FileNotFoundException("Failed to ensure directory: " +
//                            dir.getAbsolutePath());
//                if (ze.isDirectory())
//                    continue;
//                FileOutputStream fout = new FileOutputStream(file);
//                InputStream inputStream = zis.getInputStream(ze);
//                try {
//                    while ((count = inputStream.read(buffer)) != -1)
//                        fout.write(buffer, 0, count);
//                } finally {
//                    fout.close();
//                }
//                //if time should be restored as well
//                long time = ze.getTime();
//                if (time > 0)
//                    file.setLastModified(time);
//
//            }
//        } finally {
//            zis.close();
//        }
//    }
//
//    public static String combinedPathString(boolean isFile, String ... path) {
//        StringBuilder res = new StringBuilder();
//        if (isFile) {
//            for (String temp : path) {
//                res.append(temp);
//                if (!temp.contains(".")) res.append(File.separator);
//            }
//        } else {
//            for(int i = 0; i < path.length-1; i++) {
//                res.append(path[i]).append(File.separator);
//            }
//            res.append(path[path.length-1]);
//        }
//
//        return res.toString();
//    }
//
//    public static File getFileFromPath(String path) {
//        return new File(path);
//    }
//
//    public static boolean isFileExists(String path) {
//        return new File(path).exists();
//    }
//
//    public static void copyFileStream(InputStream in, OutputStream out) throws IOException {
//        byte [] buffer = new byte[1024];
//        int read;
//        while((read = in.read(buffer)) != -1) {
//            out.write(buffer, 0, read);
//        }
//    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeSampledBitmapFromFile(String path,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
}
