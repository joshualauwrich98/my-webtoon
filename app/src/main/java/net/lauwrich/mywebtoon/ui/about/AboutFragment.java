package net.lauwrich.mywebtoon.ui.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import net.lauwrich.mywebtoon.R;

import java.util.Objects;

public class AboutFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about, container, false);

        ListView lv = root.findViewById(R.id.about_lv);

        ArrayAdapter<CharSequence> aa = ArrayAdapter.createFromResource(Objects.requireNonNull(getActivity()), R.array.about_data, android.R.layout.simple_list_item_1);
        lv.setAdapter(aa);

        return root;
    }
}