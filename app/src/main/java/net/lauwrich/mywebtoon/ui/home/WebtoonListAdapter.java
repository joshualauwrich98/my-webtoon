package net.lauwrich.mywebtoon.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.lauwrich.mywebtoon.MainActivity;
import net.lauwrich.mywebtoon.R;
import net.lauwrich.mywebtoon.model.WebtoonItem;
import net.lauwrich.mywebtoon.util.FileManager;

import java.io.File;

public class WebtoonListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private WebtoonListAdapterListener listener;
    private WebtoonItem[] webtoonList;

    public WebtoonListAdapter(LayoutInflater layoutInflater, WebtoonListAdapterListener listener) {
        this.inflater = layoutInflater;
        this.listener = listener;
        updateWebtoonList();
    }

    public void updateWebtoonList() {
        this.webtoonList = FileManager.getInstance().getWebtoonItem();
    }

    @Override
    public int getCount() {
        return this.webtoonList.length;
    }

    @Override
    public Object getItem(int i) {
        return this.webtoonList[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        WebtoonListViewHolder vHolder;

        if (view == null) {
            view = this.inflater.inflate(R.layout.webtoon_list, viewGroup, false);
            TextView title = view.findViewById(R.id.webtoon_list_title);
            ImageView cover = view.findViewById(R.id.webtoon_list_cover_image);
            vHolder = new WebtoonListViewHolder(title, cover);
            view.setTag(vHolder);
        } else {
            vHolder = (WebtoonListViewHolder) view.getTag();
        }

        vHolder.getTitle().setText(this.webtoonList[i].getName());
        vHolder.getCover().setImageBitmap(this.webtoonList[i].getCoverImage());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.showWebtoonEpisodeList(webtoonList[i].getName());
            }
        });

        return view;
    }

    private class WebtoonListViewHolder {
        private TextView title;
        private ImageView cover;

        public WebtoonListViewHolder(TextView title, ImageView cover) {
            this.title = title;
            this.cover = cover;
        }

        public TextView getTitle() {
            return title;
        }

        public void setTitle(TextView title) {
            this.title = title;
        }

        public ImageView getCover() {
            return cover;
        }

        public void setCover(ImageView cover) {
            this.cover = cover;
        }
    }

    public interface WebtoonListAdapterListener {
        void showWebtoonEpisodeList(String name);
    }
}
