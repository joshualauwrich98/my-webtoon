package net.lauwrich.mywebtoon.ui.log;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import net.lauwrich.mywebtoon.MainActivity;
import net.lauwrich.mywebtoon.R;

import java.util.ArrayList;

public class LogAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<LogItem> data;
    private Activity activity;

    public LogAdapter(Activity activity) {
        this.inflater = activity.getLayoutInflater();
        this.activity = activity;
        this.data = new ArrayList<>();
        init();
    }

    private void init() {
        this.data.add(new LogItem("Launch v1.0 (2019-10-27 15:20)",
                new String[]{
                        "Full guide available on readme.md file.",
                        "3 Webtoon items available to read.",
                        "Upcoming updates will be scheduled on 2019-11-27."
                }));
        this.data.add(new LogItem("Launch v1.0 (2019-10-27 15:20)",
                new String[]{
                        "Full guide available on readme.md file.",
                        "3 Webtoon items available to read.",
                        "Upcoming updates will be scheduled on 2019-11-27."
                }));
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int i) {
        return this.data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LogViewHolder vh;

        if (view == null) {
            view = inflater.inflate(R.layout.log_layout, viewGroup, false);
            TextView title = view.findViewById(R.id.log_title);
            ListView items = view.findViewById(R.id.log_desc);
            vh = new LogViewHolder(title, items);
            view.setTag(vh);
        } else {
            vh = (LogViewHolder) view.getTag();
        }

        vh.getTitle().setText(this.data.get(i).getTitle());
        vh.getItems().setAdapter(new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, this.data.get(i).getLogs()));

        return view;
    }

    private class LogViewHolder {
        private TextView title;
        private ListView items;

        public LogViewHolder(TextView title, ListView items) {
            this.title = title;
            this.items = items;
        }

        public TextView getTitle() {
            return title;
        }

        public ListView getItems() {
            return items;
        }
    }

    private class LogItem {
        private final String title;
        private final String [] logs;

        public LogItem(String title, String[] logs) {
            this.title = title;
            this.logs = logs;
        }

        public String getTitle() {
            return title;
        }

        public String[] getLogs() {
            return logs;
        }
    }
}
