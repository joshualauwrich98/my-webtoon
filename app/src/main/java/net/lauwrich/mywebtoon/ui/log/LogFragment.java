package net.lauwrich.mywebtoon.ui.log;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import net.lauwrich.mywebtoon.R;

import java.util.Objects;

public class LogFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_log, container, false);

        ((ListView)root.findViewById(R.id.log_lv)).setAdapter(new LogAdapter(Objects.requireNonNull(getActivity())));

        return root;
    }
}