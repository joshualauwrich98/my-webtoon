package net.lauwrich.mywebtoon.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.lauwrich.mywebtoon.R;
import net.lauwrich.mywebtoon.util.FileManager;

public class EpisodeListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private String [] episodes;
    private EpisodeListListener listener;
    private String webtoon;

    public EpisodeListAdapter(LayoutInflater inflater, EpisodeListListener listener, String webtoon) {
        this.inflater = inflater;
        this.listener = listener;
        this.webtoon = webtoon;
        updateEpisodeList();
    }

    public void updateEpisodeList() {
        this.episodes = FileManager.getInstance().getEpisodeList(this.webtoon);
        notifyDataSetChanged();
    }

    public void updateEpisodeListFromKeyword(String keyword) {
        this.episodes = FileManager.getInstance().getEpisodeList(this.webtoon, keyword);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.episodes.length;
    }

    @Override
    public Object getItem(int i) {
        return this.episodes[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        EpisodeListViewHolder viewHolder;

        if (view == null) {
            view = inflater.inflate(R.layout.episode_list, viewGroup, false);
            TextView title = view.findViewById(R.id.home_episode_item);
            viewHolder = new EpisodeListViewHolder(title);
            view.setTag(viewHolder);
        } else {
            viewHolder = (EpisodeListViewHolder) view.getTag();
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.goToReader(webtoon, episodes[i]);
            }
        });
        viewHolder.getTitle().setText(this.episodes[i]);

        return view;
    }

    public class EpisodeListViewHolder {
        private TextView title;

        public TextView getTitle() {
            return title;
        }

        public void setTitle(TextView title) {
            this.title = title;
        }

        public EpisodeListViewHolder(TextView title) {
            this.title = title;
        }
    }

    public interface EpisodeListListener {
        void goToReader(String webtoon, String episode);
    }
}
