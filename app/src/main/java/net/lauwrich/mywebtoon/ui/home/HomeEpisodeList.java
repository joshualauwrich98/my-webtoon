package net.lauwrich.mywebtoon.ui.home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import net.lauwrich.mywebtoon.R;

public class HomeEpisodeList extends Fragment implements EpisodeListAdapter.EpisodeListListener {

    private EpisodeListListener mListener;
    private String name;
    private EpisodeListAdapter adapter;
    private ListView episodeList;
    private EditText searchInput;

    public static HomeEpisodeList newInstance(EpisodeListListener listener, String name) {
        HomeEpisodeList fragment = new HomeEpisodeList();
        fragment.mListener = listener;
        fragment.name = name;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_episode_list, container, false);

        ((TextView) view.findViewById(R.id.home_episode_title)).setText(this.name);
        view.findViewById(R.id.home_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.changeToWebtoonList();
            }
        });
        this.episodeList = view.findViewById(R.id.home_episode_list);
        this.adapter = new EpisodeListAdapter(getLayoutInflater(), this, this.name);
        this.episodeList.setAdapter(this.adapter);
        this.searchInput = view.findViewById(R.id.search_input);
        this.searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.updateEpisodeListFromKeyword(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Override
    public void goToReader(String webtoon, String episode) {
        mListener.goToReader(webtoon,episode);
    }

    public interface EpisodeListListener {
        void changeToWebtoonList();
        void goToReader(String webtoon, String episode);
    }
}
