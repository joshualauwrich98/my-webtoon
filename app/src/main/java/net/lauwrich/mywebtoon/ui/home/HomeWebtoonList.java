package net.lauwrich.mywebtoon.ui.home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import net.lauwrich.mywebtoon.R;

public class HomeWebtoonList extends Fragment implements WebtoonListAdapter.WebtoonListAdapterListener {

    private WebtoonListListener mListener;
    private WebtoonListAdapter webtoonListAdapter;
    private ListView listView;

    public static HomeWebtoonList newInstance(WebtoonListListener listListener) {
        HomeWebtoonList res = new HomeWebtoonList();
        res.mListener = listListener;
        return res;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home_webtoon_list, container, false);
        this.listView = root.findViewById(R.id.home_webtoon_list);
        webtoonListAdapter = new WebtoonListAdapter(getLayoutInflater(), this);
        this.listView.setAdapter(webtoonListAdapter);

        return root;
    }

    @Override
    public void showWebtoonEpisodeList(String name) {
        mListener.changeToEpisodeFragment(name);
    }

    public interface WebtoonListListener {
        void changeToEpisodeFragment(String name);
    }
}
