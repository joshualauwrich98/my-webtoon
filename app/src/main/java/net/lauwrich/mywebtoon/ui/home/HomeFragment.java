package net.lauwrich.mywebtoon.ui.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import net.lauwrich.mywebtoon.R;
import net.lauwrich.mywebtoon.ReaderActivity;

import java.io.File;

public class HomeFragment extends Fragment implements HomeWebtoonList.WebtoonListListener, HomeEpisodeList.EpisodeListListener {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        changeToWebtoonList();

        return root;
    }

    @Override
    public void changeToWebtoonList() {
        getChildFragmentManager().beginTransaction().replace(R.id.home_child_frag_container, HomeWebtoonList.newInstance(this)).commit();
    }

    @Override
    public void goToReader(String webtoon, String episode) {
        Intent intent = new Intent(getActivity(), ReaderActivity.class);
        intent.putExtra("webtoon", webtoon);
        intent.putExtra("episode", episode);
        startActivity(intent);
    }

    @Override
    public void changeToEpisodeFragment(String name) {
        getChildFragmentManager().beginTransaction().replace(R.id.home_child_frag_container, HomeEpisodeList.newInstance(this, name)).commit();
    }
}