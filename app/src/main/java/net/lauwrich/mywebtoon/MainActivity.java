package net.lauwrich.mywebtoon;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import net.lauwrich.mywebtoon.util.FileManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private long backPressedTime;
    private BottomNavigationView navView;

    /* Constants for requesting permissions */
    private static final int ALL_REQUEST_PERMISSION_CODE = 34198;
    private static final String [] PERMISSION_NEEDED = new String[] {
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        if(!isAllPermissionGranted()) {
            sendRequestPermission();
        } else {
            FileManager.getInstance().init();
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
                @Override
                public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                    controller.popBackStack();
                }
            });
            NavigationUI.setupWithNavController(navView, navController);
        }
        this.backPressedTime = 0;
    }

    private List<String> getPermissionList() {
        List<String> permissions = new ArrayList<>();
        for(String permission: PERMISSION_NEEDED) {
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                permissions.add(permission);
        }

        return permissions;
    }

    private boolean isAllPermissionGranted() {
        return getPermissionList().size() == 0;
    }

    private void sendRequestPermission() {
        List<String> permissionsTemp = getPermissionList();
        String [] permissionLists = new String[permissionsTemp.size()];
        permissionsTemp.toArray(permissionLists);

        ActivityCompat.requestPermissions(this, permissionLists, ALL_REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int i, @NonNull String[] strings, @NonNull int[] ints) {
        // If request is cancelled, the result arrays are empty.
        if (i == ALL_REQUEST_PERMISSION_CODE) {
            Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show();
            FileManager.getInstance().init();
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
                @Override
                public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                    controller.popBackStack();
                }
            });
            NavigationUI.setupWithNavController(navView, navController);
//            if (ints.length > 0
//                    && ints[0] == PackageManager.PERMISSION_GRANTED) {
//                final Snackbar result = Snackbar.make(effectLayout, R.string.snackbar_permission_success, Snackbar.LENGTH_LONG);
//                result.setAction(R.string.snackbar_dismissed_text, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        result.dismiss();
//                    }
//                });
//                result.setActionTextColor(Color.WHITE);
//                result.getView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                result.show();
//            } else {
//                sendRequestPermission();
//            }
        }
    }

    @Override
    public void onBackPressed() {
            if (this.backPressedTime == 0 || System.currentTimeMillis() - this.backPressedTime > 1500) {
                this.backPressedTime = System.currentTimeMillis();
                Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
            } else {
                super.onBackPressed();
            }
    }
}
