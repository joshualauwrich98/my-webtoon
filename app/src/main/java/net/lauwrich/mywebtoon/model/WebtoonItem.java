package net.lauwrich.mywebtoon.model;

import android.graphics.Bitmap;

public class WebtoonItem implements Comparable<WebtoonItem> {
    private final String name;
    private final Bitmap coverImage;

    public WebtoonItem(String name, Bitmap coverImage) {
        this.name = name;
        this.coverImage = coverImage;
    }

    public String getName() {
        return name;
    }

    public Bitmap getCoverImage() {
        return coverImage;
    }

    @Override
    public int compareTo(WebtoonItem webtoonItem) {
        return this.name.compareTo(webtoonItem.getName());
    }
}
